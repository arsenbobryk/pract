﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maryanka
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string login = textBoxLogin.Text;
            string pass = textBoxPassword.Text;
            pass = HashFunc.CalculateMD5Hash(pass);
            if (userExist(login, pass) && login == "admin")
            {
                MessageBox.Show("Welcome back admin!");
                Form2 mainForm = new Form2(login);
                mainForm.Show();
                mainForm.FormClosing += new FormClosingEventHandler(mainForm_closing);
                (this).Hide();
            }
            else if(userExist(login, pass))
            {
                if (userIsBanned(login))
                {
                    MessageBox.Show("Sorry...u are banned");
                    return;
                }
                MessageBox.Show("Welcome back user!");
                Form1 mainForm = new Form1(login);
                mainForm.Show();
                mainForm.FormClosing += new FormClosingEventHandler(mainForm_closing);
                (this).Hide();
            }
            else
            {
                MessageBox.Show("User doesn't exist...");
            }
        }

        private void mainForm_closing(object sender, FormClosingEventArgs e)
        {
            Close();
        }

        private bool userExist(string login, string pass)
        {
            string query = $"SELECT login FROM users where login='{login}' and pass='{pass}'";
            DataTable DT = DBFunc.sendRequest(query);
            return (DT.Rows.Count != 0);
        }

        private bool userIsBanned(string login)
        {
            string query = $"SELECT login FROM banned where login='{login}'";
            DataTable DT = DBFunc.sendRequest(query);
            return (DT.Rows.Count != 0);
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            Loading loading = new Loading();
            loading.Show();

            loading.FormClosing += new FormClosingEventHandler(formClosing);
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
        }

        private void formClosing(object sender, FormClosingEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            this.CenterToScreen();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Registration reg = new Registration();
            reg.Show();
        }
    }
}
