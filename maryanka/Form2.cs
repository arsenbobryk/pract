﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace maryanka
{
    public partial class Form2 : Form
    {
        string login = "";
        DataTable items = new DataTable();
        public Form2(string _login)
        {
            InitializeComponent();
            login = _login;
            clearData();
        }

        private void clearData()
        {
            dataGridView1.ClearSelection();
            foreach (TextBox txt in this.Controls.OfType<TextBox>())
            {
                txt.Text = "";
            }
        }

        private void buttonLog_out_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool ValidatePassword(string password, out string ErrorMessage)
        {
            var input = password;
            ErrorMessage = string.Empty;

            if (string.IsNullOrWhiteSpace(input))
            {
                throw new Exception("Password should not be empty...");
            }

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=[{]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(input))
            {
                ErrorMessage = "Password should contain at least one lower case letter...";
                return false;
            }
            else if (!hasUpperChar.IsMatch(input))
            {
                ErrorMessage = "Password should contain at least one upper case letter...";
                return false;
            }
            else if (!hasMiniMaxChars.IsMatch(input))
            {
                ErrorMessage = "Password should not be lesser than 8 or greater than 15 characters...";
                return false;
            }
            else if (!hasNumber.IsMatch(input))
            {
                ErrorMessage = "Password should contain at least one numeric value...";
                return false;
            }

            //else if (!hasSymbols.IsMatch(input))
            //{
            //    ErrorMessage = "Password should contain at least one special case character...";
            //    return false;
            //}
            else
            {
                return true;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private bool checkLogin(string login)
        {
            string query = $"SELECT login FROM users where login='{login}'";
            DataTable DT = DBFunc.sendRequest(query);
            return (DT.Rows.Count == 0);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            loadItems();
        }

        private void loadItems()
        {
            items = DBFunc.sendRequest($"SELECT login, pass, email, firstname, lastname FROM users");
            dataGridView1.DataSource = items;
            dataGridView1.Columns[0].Visible = false;
        }

        private void loadUsersList()
        {
            var users = DBFunc.sendRequest($"SELECT login, pass, email, firstname, lastname FROM users");
            dataGridView1.DataSource = users;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string login = textBox1.Text;
            string pass = textBox2.Text;
            string email = textBox3.Text;
            string firstname = textBox4.Text;
            string lastname = textBox5.Text;
            try
            {
                if (this.dataGridView1.SelectedRows.Count == 0)
                    return;
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                login = row.Cells[1].Value.ToString();
                pass = row.Cells[2].Value.ToString();
                email = row.Cells[3].Value.ToString();
                firstname = row.Cells[4].Value.ToString();
                lastname = row.Cells[5].Value.ToString();
            }
            catch (Exception ex)
            { MessageBox.Show("Invalid data"); }
            loadItems();
        }

        private void Ban_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            if (id == "")
                return;
            try
            {
                DBFunc.sendRequest($"INSERT INTO `banned`(`login`) VALUES('{login}')");
                loadUsersList();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            string login = textBox1.Text;
            string pass = textBox2.Text;
            string fName = textBox4.Text;
            string lName = textBox5.Text;
            string email = textBox3.Text;

            if (IsValidEmail(email) == false)
            {
                MessageBox.Show("You entered invalid e-mail...");
                return;
            }

            string errorMessage = "";

            if (ValidatePassword(pass, out errorMessage) == false)
            {
                MessageBox.Show(errorMessage);
                return;
            }

            if (checkLogin(login) == false)
            {
                MessageBox.Show("You entered existing login...");
                return;
            }
            string query = $"INSERT INTO users values(0, '{login}', '{HashFunc.CalculateMD5Hash(pass)}', '{email}', '{fName}', '{lName}')";
            DBFunc.sendRequest(query);
        }

        private void buttonActivate_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            if (id == "")
                return;
            try
            {
                DBFunc.sendRequest($"DELETE FROM `banned`(`login`) VALUES('{login}')");
                loadUsersList();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void Form2_Load_1(object sender, EventArgs e)
        {
            loadUsersList();
        }
    }
}

