﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maryanka
{
    public partial class Form1 : Form
    {
        string login = "";
        DataTable items = new DataTable();
        public Form1(string _login)
        {
            InitializeComponent();
            login = _login;
            clearData();
        }

        private void clearData()
        {
            dataGridView1.ClearSelection();
            foreach (TextBox txt in this.Controls.OfType<TextBox>())
            {
                txt.Text = "";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadItems();
        }

        private void loadItems()
        {
            items = DBFunc.sendRequest($"SELECT id, surname, adress, group_name, grades, phone FROM data WHERE user_login='{login}'");
            dataGridView1.DataSource = items;
            dataGridView1.Columns[0].Visible = false;
        }

        private void buttonDelete_Click_Click(object sender, EventArgs e)
        {
            try
            {
                string id = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                if (id == "")
                    return;
                dataGridView1.Rows.RemoveAt(this.dataGridView1.SelectedRows[0].Index);
                DBFunc.sendRequest($"DELETE FROM data WHERE id='{id}'");

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            clearData();
        }

        private void buttonEdit_Click_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dataGridView1.SelectedRows.Count == 0)
                    return;

                DataGridViewRow row = dataGridView1.SelectedRows[0];
                List<int> grd = new List<int>();
                string gradesss = textBox5.Text;
                string[] aa = gradesss.Split(' ');
                foreach (var item in aa)
                {
                    grd.Add(Int32.Parse(item.ToString()));
                }
                string surname = textBox1.Text;
                string phone = textBox2.Text;
                string adress = textBox3.Text;
                string group_name = textBox4.Text;
                string csv = String.Join(" ", grd);
                Student adr = new Student(textBox1.Text, textBox4.Text, textBox2.Text, textBox3.Text, grd);

                string id = row.Cells[0].Value.ToString();
                if (id == "")
                    return;
                DBFunc.sendRequest($"UPDATE `data` SET `surname`='{surname}',`group_name`='{group_name}',`grades`='{csv}',`phone`='{phone}',`user_login`='{login}',`adress`='{adress}' WHERE id='{id}'");
                loadItems();
            }
            catch (Exception ex)
            { MessageBox.Show("Invalid data"); }
            clearData();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string surname = textBox1.Text;
            string phone = textBox2.Text;
            string adress = textBox3.Text;
            string group_name = textBox4.Text;
            try
            {
                if (this.dataGridView1.SelectedRows.Count == 0)
                    return;
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                surname = row.Cells[1].Value.ToString();
                phone = row.Cells[2].Value.ToString();
                adress = row.Cells[3].Value.ToString();
                group_name = row.Cells[4].Value.ToString();
                textBox5.Text = row.Cells[5].Value.ToString();
            }
            catch (Exception ex)
            { MessageBox.Show("Invalid data"); }
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Click(object sender, EventArgs e)
        {
             try
            {
                List<int> grd = new List<int>();
                string gradesss = textBox5.Text;
                string[] aa = gradesss.Split(' ');
                foreach (var item in aa)
                {
                    grd.Add(Int32.Parse(item.ToString()));
                }
                Student adr = new Student(textBox1.Text, textBox4.Text, textBox2.Text, textBox3.Text, grd);

                string surname = textBox1.Text;
                string phone = textBox2.Text;
                string adress = textBox3.Text;
                string group_name = textBox4.Text;
                string csv = String.Join(" ", grd);
                DBFunc.sendRequest($"INSERT INTO `data`(`ID`, `surname`, `group_name`, `grades`, `phone`, `user_login`, `adress`) VALUES (0,'{surname}','{group_name}','{csv}','{phone}','{login}','{adress}')");
                loadItems();
            }
            catch (Exception ex)
            { MessageBox.Show("Invalid data" + ex.Message); }
            clearData();
        }
    }
}
