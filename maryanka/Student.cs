﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace maryanka
{
    class Student
    {
        [Required, RegularExpression("/^[A-Za-z]+$/")]
        public string surname { get; }
        [Required, RegularExpression("/^[A-Za-z]+$/")]
        public string groupName { get; }
        [Required, RangeAttribute(0, 5)]
        public List<int> grades { get; }
        public string phone { get; }
        [Required, RegularExpression("/^[A-Za-z]+$/")]
        public string adress { get; }


        public Student(string s, string g, string p, string a, List<int> gg)
        {
            this.surname = s;
            this.groupName = g;
            this.phone = p;
            this.adress = a;
            this.grades = gg;
            if (string.IsNullOrWhiteSpace(surname) || string.IsNullOrWhiteSpace(groupName) || string.IsNullOrWhiteSpace(phone) || string.IsNullOrWhiteSpace(adress) || grades.Count != 5)
                throw new ValidationException("Wrong input, empty space...");
        }
    }
}
